//
//  PainButton.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit

class PainButton: UIButton
{
    fileprivate let level: Int
    
    init(level: Int)
    {
        self.level = level
        super.init(frame: .zero)
        
        switch self.level {
        case 0:
            self.setTitle("😄", for: .normal)
        case 1:
            self.setTitle("☺️", for: .normal)
        case 2:
            self.setTitle("🙂", for: .normal)
        case 3:
            self.setTitle("😡", for: .normal)
        case 4:
            self.setTitle("🤬", for: .normal)
            
        default:()
        }
        
        self.backgroundColor = UIColor.lightGray
        self.layer.cornerRadius = 6.5
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
}
