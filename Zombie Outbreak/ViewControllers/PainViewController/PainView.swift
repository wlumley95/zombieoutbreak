//
//  PainView.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit
import SnapKit

class PainView: ZombieOutbreakView
{
    //MARK: - Properties
    public let titleLabel: UILabel =
    {
        let label = UILabel(frame: .zero)
        label.text = "Select severity level:"
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.boldFont(size: 23)
        
        return label
    }()
    
    public let illnessLabel: UILabel =
    {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.subtitle
        
        return label
    }()
    
    public let level0Button: PainButton =
    {
        let painButton = PainButton(level: 0)
        return painButton
    }()

    public let level1Button: PainButton =
    {
        let painButton = PainButton(level: 1)
        return painButton
    }()

    public let level2Button: PainButton =
    {
        let painButton = PainButton(level: 2)
        return painButton
    }()

    public let level3Button: PainButton =
    {
        let painButton = PainButton(level: 3)
        return painButton
    }()

    public let level4Button: PainButton =
    {
        let painButton = PainButton(level: 4)
        return painButton
    }()

    //MARK: - UIView
    override func setupConstraints()
    {
        self.views = [
            self.titleLabel,
            self.illnessLabel,
            self.level0Button,
            self.level1Button,
            self.level2Button,
            self.level3Button,
            self.level4Button
        ]
        
        self.titleLabel.snp.makeConstraints({(make) in
            make.leading.equalTo(Values.UI.leadingMargin)
            make.topMargin.equalTo(Values.UI.topMargin)
        })

        self.illnessLabel.snp.makeConstraints({(make) in
            make.leading.equalTo(Values.UI.leadingMargin)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(4.0)
        })

        self.level0Button.snp.makeConstraints({(make) in
            make.leading.equalTo(Values.UI.leadingMargin)
            make.top.equalTo(self.illnessLabel.snp.bottom).offset(20.0)
            make.width.equalTo(65)
            make.height.equalTo(75)
        })
        
        self.level1Button.snp.makeConstraints({(make) in
            make.leading.equalTo(self.level0Button.snp.trailing).offset(4.0)
            make.top.equalTo(self.level0Button.snp.top)
            make.width.equalTo(65)
            make.height.equalTo(75)
        })
        
        self.level2Button.snp.makeConstraints({(make) in
            make.leading.equalTo(self.level1Button.snp.trailing).offset(4.0)
            make.top.equalTo(self.level0Button.snp.top)
            make.width.equalTo(65)
            make.height.equalTo(75)
        })
        
        self.level3Button.snp.makeConstraints({(make) in
            make.leading.equalTo(self.level2Button.snp.trailing).offset(4.0)
            make.top.equalTo(self.level0Button.snp.top)
            make.width.equalTo(65)
            make.height.equalTo(75)
        })
        
        self.level4Button.snp.makeConstraints({(make) in
            make.leading.equalTo(self.level3Button.snp.trailing).offset(4.0)
            make.top.equalTo(self.level0Button.snp.top)
            make.width.equalTo(65)
            make.height.equalTo(75)
        })
    }
}
