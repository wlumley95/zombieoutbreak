//
//  PainViewController.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit
import SVProgressHUD

class PainViewController: UIViewController
{
    internal unowned var painView: PainView { return self.view as! PainView }
    
    internal unowned var titleLabel: UILabel { return self.painView.titleLabel }
    internal unowned var illnessLabel: UILabel { return self.painView.illnessLabel }

    internal unowned var level0Button: PainButton { return self.painView.level0Button }
    internal unowned var level1Button: PainButton { return self.painView.level1Button }
    internal unowned var level2Button: PainButton { return self.painView.level2Button }
    internal unowned var level3Button: PainButton { return self.painView.level3Button }
    internal unowned var level4Button: PainButton { return self.painView.level4Button }

    ///The illness that the user selected in the previous screen
    fileprivate let illness: Illness
    
    fileprivate var hospitals = [Hospital]()
    
    //MARK: - UIViewController
    init(illness: Illness)
    {
        self.illness = illness
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
        
    override func loadView()
    {
        self.view = PainView()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.illnessLabel.text = illness.name
        
        self.level0Button.addTarget(self, action: #selector(level0ButtonTapped), for: .touchUpInside)
        self.level1Button.addTarget(self, action: #selector(level1ButtonTapped), for: .touchUpInside)
        self.level2Button.addTarget(self, action: #selector(level2ButtonTapped), for: .touchUpInside)
        self.level3Button.addTarget(self, action: #selector(level3ButtonTapped), for: .touchUpInside)
        self.level4Button.addTarget(self, action: #selector(level4ButtonTapped), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.hospitals.removeAll()
    }
    
    fileprivate func painSelected(href: String?, level: Int)
    {
        SVProgressHUD.show()
        
        self.getHospitals(href: href, level: level, onSuccess: {(newHref) in
            self.painSelected(href: newHref, level: level)
        })
    }
    
    fileprivate func getHospitals(href: String?, level: Int, onSuccess: ((_ href: String) -> Void)?)
    {
        APICalls.getHospitals(href: href, onSuccess: {(json) in
            SVProgressHUD.dismiss()
            
            var hospitals = [Hospital]()
            
            let hospitalJSONs = json["_embedded"]["hospitals"].arrayValue
            for hospitalJSON in hospitalJSONs {
                let id = hospitalJSON["id"].intValue
                let name = hospitalJSON["name"].stringValue
                let waitingListJSONs = hospitalJSON["waitingList"].arrayValue
                
                var waitingListItems = [WaitingListItem]()
                for waitingListJSON in waitingListJSONs {
                    let levelOfPain = waitingListJSON["levelOfPain"].intValue
                    let averageProcessTime = waitingListJSON["averageProcessTime"].intValue
                    let patientCount = waitingListJSON["patientCount"].intValue
                    
                    let waitingListItem = WaitingListItem(patientCount: patientCount, levelOfPain: levelOfPain, averageProcessTime: averageProcessTime)
                    waitingListItems.append(waitingListItem)
                }
                
                let hospital = Hospital(id: id, name: name, waitingList: waitingListItems)
                hospitals.append(hospital)
            }
            
            self.hospitals.append(contentsOf: hospitals)
                        
            self.hospitals.sort(by: {(a, b) in
                return a.waitingTime(for: level) < b.waitingTime(for: level)
            })
            
            //If there is a nextHref, we have more hospitals to grab
            var nextHref = json["_links"]["next"]["href"].string
            
            let hospitalViewController = HospitalsViewController(illness: self.illness, hospitals: self.hospitals, painLevel: level, painViewController: self, nextHref: nextHref)
            self.present(hospitalViewController, animated: true, completion: nil)

        }, onFailure: {(error) in
            SVProgressHUD.showError(withStatus: error)
        }).execute()
    }
}

//MARK: - Actions
extension PainViewController
{
    @objc
    fileprivate func level0ButtonTapped()
    {
        self.painSelected(href: nil, level: 0)
    }
    
    @objc
    fileprivate func level1ButtonTapped()
    {
        self.painSelected(href: nil, level: 1)
    }
    
    @objc
    fileprivate func level2ButtonTapped()
    {
        self.painSelected(href: nil, level: 2)
    }
    
    @objc
    fileprivate func level3ButtonTapped()
    {
        self.painSelected(href: nil, level: 3)
    }
    
    @objc
    fileprivate func level4ButtonTapped()
    {
        self.painSelected(href: nil, level: 4)
    }
}
