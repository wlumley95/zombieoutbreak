//
//  ZombieOutbreakView.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 15/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit

class ZombieOutbreakView: UIView
{
    ///All of the subviews in this view
    internal var views = [UIView]()
    {
        didSet {
            for view in self.views {
                view.translatesAutoresizingMaskIntoConstraints = false
                self.addSubview(view)
            }
        }
    }
    
    //MARK: - UIView
    init()
    {
        super.init(frame: CGRect.zero)
        
        self.setup()
        self.setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        self.setup()
        self.setupConstraints()
    }
    
    fileprivate func setup()
    {
        self.clipsToBounds = true
        self.backgroundColor = UIColor.white
    }
    
    internal func setupConstraints()
    {
        fatalError("setupContraints() should never be called by the ZombieOutbreakView superclass.")
    }
}
