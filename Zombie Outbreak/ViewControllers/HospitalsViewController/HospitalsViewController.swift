//
//  HospitalsViewController.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 17/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit
import CoreData
import SVProgressHUD

class HospitalsViewController: UIViewController
{
    internal unowned var hospitalsView: HospitalsView { return self.view as! HospitalsView }
    
    internal unowned var titleLabel: UILabel { return self.hospitalsView.titleLabel }
    internal unowned var hospitalsTableView: UITableView { return self.hospitalsView.hospitalsTableView }

    ///The illness that the user selected
    fileprivate let illness: Illness

    ///The hospitals that the user needs to see
    fileprivate var hospitals: [Hospital]

    ///The pain level the user has selected
    fileprivate let painLevel: Int

    fileprivate let painViewController: PainViewController
    
    fileprivate var nextHref: String?

    ///The cell ID used for our IllnessCell's
    fileprivate let cellID = "HospitalCellID"
    
    //MARK: - UIViewController
    init(illness: Illness, hospitals: [Hospital], painLevel: Int, painViewController: PainViewController, nextHref: String?)
    {
        self.illness = illness
        self.hospitals = hospitals
        self.painLevel = painLevel
        self.painViewController = painViewController
        self.nextHref = nextHref
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView()
    {
        self.view = HospitalsView()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.hospitalsTableView.register(HospitalCell.self, forCellReuseIdentifier: self.cellID)
        self.hospitalsTableView.dataSource = self
        self.hospitalsTableView.delegate = self
        
        if let href = self.nextHref {
            self.getHospitals(href: href, level: self.painLevel, onSuccess: nil)
        }
    }
    
    fileprivate func getHospitals(href: String?, level: Int, onSuccess: ((_ href: String) -> Void)?)
    {
        APICalls.getHospitals(href: href, onSuccess: {(json) in
            SVProgressHUD.dismiss()
            
            var hospitals = [Hospital]()
            
            let hospitalJSONs = json["_embedded"]["hospitals"].arrayValue
            for hospitalJSON in hospitalJSONs {
                let id = hospitalJSON["id"].intValue
                let name = hospitalJSON["name"].stringValue
                let waitingListJSONs = hospitalJSON["waitingList"].arrayValue
                
                var waitingListItems = [WaitingListItem]()
                for waitingListJSON in waitingListJSONs {
                    let levelOfPain = waitingListJSON["levelOfPain"].intValue
                    let averageProcessTime = waitingListJSON["averageProcessTime"].intValue
                    let patientCount = waitingListJSON["patientCount"].intValue
                    
                    let waitingListItem = WaitingListItem(patientCount: patientCount, levelOfPain: levelOfPain, averageProcessTime: averageProcessTime)
                    waitingListItems.append(waitingListItem)
                }
                
                let hospital = Hospital(id: id, name: name, waitingList: waitingListItems)
                hospitals.append(hospital)
            }
            
            self.hospitals.append(contentsOf: hospitals)
            
            self.nextHref = json["_links"]["next"]["href"].string
            
            self.hospitals.sort(by: {(a, b) in
                return a.waitingTime(for: level) < b.waitingTime(for: level)
            })
            
            self.hospitalsTableView.reloadData()
            
        }, onFailure: {(error) in
            SVProgressHUD.showError(withStatus: error)
        }).execute()
    }
}

//MARK: - UITableView DataSource
extension HospitalsViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.hospitals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let hospital = self.hospitals[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID, for: indexPath) as! HospitalCell
        cell.configure(with: hospital, painLevel: self.painLevel)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let lastHospital = self.hospitals.count - 1
        
        //If the user has hit the last hospital
        if indexPath.row == lastHospital {
            self.getHospitals(href: self.nextHref, level: self.painLevel, onSuccess: nil )
        }
    }
}

//MARK: - UITableView Delegate
extension HospitalsViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let hospital = self.hospitals[indexPath.row]
        
        let context = DataManager.newBackgroundContext()
        context.performAndWait({
            let patientEntry = PatientEntry(context: context)
            patientEntry.hospitalName = hospital.name
            patientEntry.illnessName  = self.illness.name
            patientEntry.painLevel    = Int16(self.painLevel)
            patientEntry.waitingMins  = Int16(hospital.waitingTime(for: self.painLevel))
            
            _ = DataManager.sharedInstance.save(context: context)
        })
        
        SVProgressHUD.showSuccess(withStatus: "Stored patient entry in database")
        self.dismiss(animated: true, completion: {
            self.painViewController.dismiss(animated: true, completion: nil)
        })
    }
}
