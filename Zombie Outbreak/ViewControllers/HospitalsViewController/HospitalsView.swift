//
//  HospitalsView.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 17/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit
import SnapKit

class HospitalsView: ZombieOutbreakView
{
    //MARK: - Properties
    public let titleLabel: UILabel =
    {
        let label = UILabel(frame: .zero)
        label.text = "Our suggested Hospitals:"
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.boldFont(size: 23)
        
        return label
    }()
    
    public let hospitalsTableView: UITableView =
    {
        let tableView = UITableView(frame: .zero)
        return tableView
    }()
    
    //MARK: - UIView
    override func setupConstraints()
    {
        self.views = [
            self.titleLabel,
            self.hospitalsTableView
        ]
        
        self.titleLabel.snp.makeConstraints({(make) in
            make.leading.equalTo(Values.UI.leadingMargin)
            make.topMargin.equalTo(Values.UI.topMargin)
        })
        
        self.hospitalsTableView.snp.makeConstraints({(make) in
            make.leading.equalTo(Values.UI.leadingMargin)
            make.trailing.equalTo(-Values.UI.trailingMargin)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(12.0)
            make.bottom.equalTo(Values.UI.bottomMargin)
        })
    }
}
