//
//  HospitalCell.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 17/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit

class HospitalCell: UITableViewCell
{
    //MARK: - Properties
    public let nameLabel: UILabel =
    {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.standard
        label.textColor = UIColor.black
        label.numberOfLines = 0

        return label
    }()

    public let waitTimeLabel: UILabel =
    {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.standardFont(size: 15.0)
        label.textColor = UIColor.black
        label.numberOfLines = 0
        
        return label
    }()

    //MARK: - UITableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
        self.setupContraints()
    }
    
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        self.setup()
        self.setupContraints()
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func setup()
    {

    }
    
    fileprivate func setupContraints()
    {
        let views = [
            self.nameLabel,
            self.waitTimeLabel
        ]
        
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addSubview(view)
        }
        
        self.nameLabel.snp.makeConstraints({(make) in
            make.leading.equalToSuperview().offset(Values.UI.leadingMargin)
            make.trailing.equalToSuperview().offset(-Values.UI.trailingMargin)
            make.top.equalToSuperview().offset(10.0)
        })
        
        self.waitTimeLabel.snp.makeConstraints({(make) in
            make.leading.equalToSuperview().offset(Values.UI.leadingMargin)
            make.trailing.equalToSuperview().offset(-Values.UI.trailingMargin)
            make.top.equalTo(self.nameLabel.snp.bottom)
            make.bottom.equalToSuperview().offset(-10.0)
        })
    }

    internal func configure(with hospital: Hospital, painLevel: Int)
    {
        self.nameLabel.text = hospital.name
        self.waitTimeLabel.text = "Wait time: \(hospital.waitingTimeDescription(for: painLevel))"
    }
}
