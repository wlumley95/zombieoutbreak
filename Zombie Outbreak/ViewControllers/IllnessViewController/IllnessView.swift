//
//  IllnessView.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 12/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit
import SnapKit

class IllnessView: ZombieOutbreakView
{
    //MARK: - Properties
    public let titleLabel: UILabel =
    {
        let label = UILabel(frame: .zero)
        label.text = "Select an Illness:"
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.title
        
        return label
    }()
    
    public let illnessTableView: UITableView =
    {
        let tableView = UITableView(frame: .zero)
        return tableView
    }()
    
    public let illessRefreshControl: UIRefreshControl =
    {
        let refreshControl = UIRefreshControl()
        return refreshControl
    }()

    //MARK: - UIView
    override func setupConstraints()
    {
        self.views = [
            self.titleLabel,
            self.illnessTableView
        ]
        
        self.titleLabel.snp.makeConstraints({(make) in
            make.leading.equalTo(Values.UI.leadingMargin)
            make.topMargin.equalTo(Values.UI.topMargin)
        })
        
        self.illnessTableView.snp.makeConstraints({(make) in
            make.leading.equalTo(Values.UI.leadingMargin)
            make.trailing.equalTo(-Values.UI.trailingMargin)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(12.0)
            make.bottom.equalTo(Values.UI.bottomMargin)
        })
    }
}
