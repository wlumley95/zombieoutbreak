//
//  ViewController.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 12/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit
import SVProgressHUD

class IllnessViewController: UIViewController
{
    internal unowned var illnessView: IllnessView { return self.view as! IllnessView }
    
    internal unowned var titleLabel: UILabel { return self.illnessView.titleLabel }
    internal unowned var illnessTableView: UITableView { return self.illnessView.illnessTableView }
    internal unowned var illnessRefreshControl: UIRefreshControl { return self.illnessView.illessRefreshControl }
    
    ///The cell ID used for our IllnessCell's
    fileprivate let cellID = "IllnessCellID"
    
    ///A list of the illnesses we have received from the API
    fileprivate var illnesses = [Illness]()
    {
        didSet {
            self.illnessTableView.reloadData()
        }
    }
    
    //MARK: - UIViewController
    override func loadView()
    {
        self.view = IllnessView()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.illnessRefreshControl.addTarget(self, action: #selector(loadIllnesses), for: .valueChanged)
        self.illnessTableView.addSubview(self.illnessRefreshControl)
        
        self.illnessTableView.register(IllnessCell.self, forCellReuseIdentifier: self.cellID)
        self.illnessTableView.dataSource = self
        self.illnessTableView.delegate = self
        
        self.loadIllnesses()
    }
    
    @objc
    fileprivate func loadIllnesses()
    {
        self.illnesses.removeAll()
        
        SVProgressHUD.show()
        APICalls.getIllnesses(onSuccess: {(json) in
            SVProgressHUD.dismiss()
            self.illnessRefreshControl.endRefreshing()
            
            let illnesses = json["_embedded"]["illnesses"].arrayValue
            for illnessData in illnesses {
                let illnessJSON = illnessData["illness"].dictionaryObject!
                
                let id = illnessJSON["id"] as! Int
                let name = illnessJSON["name"] as! String
                
                let illness = Illness(id: id, name: name)
                self.illnesses.append(illness)
            }
            
            self.illnessTableView.reloadData()
            
        }, onFailure: {(error) in
            SVProgressHUD.showError(withStatus: error)
            self.illnessRefreshControl.endRefreshing()
        }).execute()
    }
}

//MARK: - UITableView DataSource
extension IllnessViewController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.illnesses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let illness = self.illnesses[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID, for: indexPath) as! IllnessCell
        cell.configure(with: illness)
        
        return cell
    }
}

//MARK: - UITableView Delegate
extension IllnessViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let illness = self.illnesses[indexPath.row]
        
        let painViewController = PainViewController(illness: illness)
        self.present(painViewController, animated: true, completion: nil)
    }
}
