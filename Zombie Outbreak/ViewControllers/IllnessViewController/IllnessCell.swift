//
//  IllnessCell.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit

class IllnessCell: UITableViewCell
{
    //MARK: - Properties
    public let nameLabel: UILabel =
    {
        let label = UILabel(frame: .zero)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.font = UIFont.standard
        label.textColor = UIColor.black

        return label
    }()
    
    //MARK: - UITableViewCell
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
        self.setupContraints()
    }
    
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        self.setup()
        self.setupContraints()
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func setup()
    {

    }
    
    fileprivate func setupContraints()
    {
        let views = [
            self.nameLabel
        ]
        
        for view in views {
            view.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addSubview(view)
        }
        
        self.nameLabel.snp.makeConstraints({(make) in
            make.leading.equalToSuperview().offset(Values.UI.leadingMargin)
            make.centerY.equalToSuperview()
        })
    }

    internal func configure(with illness: Illness)
    {
        self.nameLabel.text = illness.name
    }
}
