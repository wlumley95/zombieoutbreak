//
//  Hospital.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation

class Hospital
{
    let id: Int
    let name: String
    let waitingList: [WaitingListItem]
    
    init(id: Int, name: String, waitingList: [WaitingListItem])
    {
        self.id = id
        self.name = name
        self.waitingList = waitingList
    }
    
    public func waitingTime(for levelOfPain: Int) -> Int
    {
        for waitingListItem in waitingList {
            if waitingListItem.levelOfPain == levelOfPain {
                return waitingListItem.averageProcessTime * waitingListItem.patientCount
            }
        }
        
        fatalError("Failed to find waiting time for pain level: \(levelOfPain)")
    }
    
    public func waitingTimeDescription(for levelOfPain: Int) -> String
    {
        for waitingListItem in waitingList {
            if waitingListItem.levelOfPain == levelOfPain {
                let waitingMins = Float(waitingListItem.averageProcessTime) * Float(waitingListItem.patientCount)
                if waitingMins < 60 {
                    return "\(waitingMins) minutes"
                }
                else {
                    let waitingHours = waitingMins / 60.0
                    return "\(waitingHours.rounded()) hours"
                }
            }
        }
        
        fatalError("Failed to find waiting time for pain level: \(levelOfPain)")
    }
}

