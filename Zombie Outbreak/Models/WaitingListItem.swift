//
//  WaitingListItem.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 17/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation

struct WaitingListItem
{
    let patientCount: Int
    let levelOfPain: Int
    let averageProcessTime: Int
}
