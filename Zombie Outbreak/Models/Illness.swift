//
//  Illness.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation

struct Illness
{
    let id: Int
    let name: String
}
