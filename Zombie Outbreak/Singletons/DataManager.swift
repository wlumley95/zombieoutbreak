//
//  DataManager.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 14/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation
import CoreData

class DataManager: NSObject
{
    public static let viewContext          = DataManager.sharedInstance.persistentContainer.viewContext
    public static let newBackgroundContext = DataManager.sharedInstance.persistentContainer.newBackgroundContext
    
    //MARK: - Singleton
    override init()
    {
        super.init();
    }
    
    static let sharedInstance : DataManager =
    {
        print("Creating DataManager.")
        
        let instance = DataManager()
        return instance
    }()

    //MARK: - CoreData Stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        
        var container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: {(storeDescription, error) in
            //print("StoreDescription: \(storeDescription)")
            if let storeURL = storeDescription.url {
                
                var prettyStoreURL = storeURL.absoluteString
                prettyStoreURL = prettyStoreURL.replacingOccurrences(of: "%20", with: "\\ ")
                prettyStoreURL = prettyStoreURL.replacingOccurrences(of: "file://", with: "")
                
                print("PersistentStore URL: \(prettyStoreURL)")
            }
            
            
            if let error = error {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error)")
            }
        })
        return container
    }()
}

//MARK: - CoreData
extension DataManager
{
    @discardableResult
    public func save(context: NSManagedObjectContext) -> Bool
    {
        if context.hasChanges {
            
            print("---------- Start Save MOC ----------")
            print("MOC Name: \(context.name ?? "nil")")
            
            self.printManagedObjects(context.insertedObjects, title: "Inserting Objects")
            self.printManagedObjects(context.updatedObjects, title: "Updating Objects")
            self.printManagedObjects(context.deletedObjects, title: "Deleting Objects")
            //self.printManagedObjects(context.registeredObjects, title: "Registered Objects")
            
            print("---------- End Save MOC ------------")
            
            do {
                try context.save()
            }
            catch let error {
                print("Could not save context, error: \(error)")
                //NSApplication.shared.presentError(error)
                return false
            }
        }
        
        return true
    }
    
    fileprivate func printManagedObjects(_ managedObjects: Set<NSManagedObject>, title: String?)
    {
        if let title = title {
            print("---\(title)---")
        }
        
        if managedObjects.count == 0 {
            print("- No objects\n")
            return
        }
        
        for object in managedObjects {
            print("- \(object)")
            
            let changes = object.changedValues()
            if changes.count == 0 {
                print("  - No Changes")
                continue
            }
            
            print("  --Changes--")
            for (key, value) in changes {
                
                var valueDesc = String(describing: value)
                
                //print("valueDesc: \(valueDesc)")
                print("valueDesc.count: \(valueDesc.count)")
                
                if valueDesc.count >= 100 {
                    valueDesc = "\(valueDesc.count) character string"
                }
                
                print("    --\(key): \(valueDesc)--")
                print("")
            }
        }
        
        print("\n")
    }
}
