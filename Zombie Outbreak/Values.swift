//
//  Values.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit

struct Values
{
    struct UI
    {
        static let leadingMargin = CGFloat(20)
        static let trailingMargin = CGFloat(20)
        static let topMargin = CGFloat(20)
        static let bottomMargin = CGFloat(20)
        static let buttonHeight = CGFloat(40)
    }
}
