//
//  APICalls.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 15/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation
import SwiftyJSON

class APICalls
{
    public static func getIllnesses(onSuccess: @escaping (_ json: JSON) -> Void, onFailure: @escaping (_ error: String) -> Void) -> NetworkTask
    {
        let networkTask = NetworkTask(uri: "illnesses", params: nil, httpMethod: .get)
        networkTask.onCompletion = {(data, response, error) in
            if let error = error { onFailure(error.localizedDescription); return }
            guard let data = data else { return }
            
            do {
                let json = try JSON(data: data)
                
                //If our status code indicates success, pass our JSON through as a success, otherwise treat it as a failure
                let statusCode = (response as! HTTPURLResponse).statusCode
                if statusCode >= 200 && statusCode < 300 {
                    onSuccess(json)
                }
                else {
                    onFailure("Failed to communicate with Zombie Outbreak network")
                }
            }
            catch _ {
                onFailure("Failed to parse network response")
            }
        }
        
        return networkTask
    }
    
    public static func getHospitals(href: String?, onSuccess: @escaping (_ json: JSON) -> Void, onFailure: @escaping (_ error: String) -> Void) -> NetworkTask
    {
        var networkTask: NetworkTask?        
        if let href = href {
            networkTask = NetworkTask(urlString: href, params: nil, httpMethod: .get)
        }
        else {
            networkTask = NetworkTask(uri: "hospitals", params: nil, httpMethod: .get)
        }
        
        networkTask?.onCompletion = {(data, response, error) in
            if let error = error { onFailure(error.localizedDescription); return }
            guard let data = data else { return }
            
            do {
                let json = try JSON(data: data)
                
                //If our status code indicates success, pass our JSON through as a success, otherwise treat it as a failure
                let statusCode = (response as! HTTPURLResponse).statusCode
                if statusCode >= 200 && statusCode < 300 {
                    onSuccess(json)
                }
                else {
                    onFailure("Failed to communicate with Zombie Outbreak network")
                }
            }
            catch _ {
                onFailure("Failed to parse network response")
            }
        }
        
        return networkTask!
    }
}
