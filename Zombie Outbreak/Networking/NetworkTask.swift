//
//  NetworkTask.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 15/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit
import SystemConfiguration

class NetworkTask
{
    fileprivate let environment = Environment.prod
    
    ///The underlying URLRequest that we will be using
    fileprivate(set) var request: URLRequest
    
    fileprivate(set) var response: URLResponse?
    fileprivate(set) var responseError: Error?
        
    ///The HTTP method that this request will be using
    fileprivate(set) var httpMethod: HttpMethod
    
    ///The closure that will be called if the request is successful
    public var onCompletion: (Data?, URLResponse?, Error?) -> Void = {(_, _, _) in}
    
    ///The closure that will be called if the request is unsuccessful
    public var failedToComplete: (String) -> Void = {(_) in}
    
    //MARK: - NetworkTask
    init(request: URLRequest)
    {
        self.request    = request
        self.httpMethod = HttpMethod(rawValue: request.httpMethod!)!
    }
    
    init(uri: String, params: Data?, httpMethod: HttpMethod)
    {
        let baseURL = self.environment.rawValue
        let urlString = baseURL + uri
        
        guard let url = URL(string: urlString) else {
            print("Could not create URL from string: \(urlString)")
            abort()
        }
        
        self.httpMethod = httpMethod
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        request.timeoutInterval = 90
        request.httpBody = params
        
        //Set the ContentType
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        self.request = request
    }

    init(urlString: String, params: Data?, httpMethod: HttpMethod)
    {
        guard let url = URL(string: urlString) else {
            print("Could not create URL from string: \(urlString)")
            abort()
        }
        
        self.httpMethod = httpMethod
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        request.timeoutInterval = 90
        request.httpBody = params
        
        //Set the ContentType
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        self.request = request
    }

    public static func hasConnection() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
            else {
                return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
        
    public func execute()
    {
        print("Sending request to URL: \(self.request)")
        
        //If we have no internet connection
        if !NetworkTask.hasConnection() {
            print("Skipping network call, no network connection.")
            
            
            DispatchQueue.main.async(execute: {
                self.failedToComplete("Device is offline.")
            })
            
            return
        }
        
        /*
         This chunk of code will bypass the iOS bug that immediately kills network connections when the app
         is pushed to the background
         */
        /*--------------------------------------------------*/
        var backgroundTask: UIBackgroundTaskIdentifier?
        backgroundTask = UIApplication.shared.beginBackgroundTask(withName: "NetworkBG", expirationHandler: {
            UIApplication.shared.endBackgroundTask(backgroundTask!)
            backgroundTask = UIBackgroundTaskIdentifier.invalid
        })
        /*--------------------------------------------------*/
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        let completionHandler = {(data : Data?, response : URLResponse?, error : Error?) in
            
            //Let the application know that the network call can stop having background rights
            UIApplication.shared.endBackgroundTask(backgroundTask!)
            backgroundTask = UIBackgroundTaskIdentifier.invalid
            
            //If we can convert the NSURLResponse to an NSHTTPURLResponse
            guard let urlResponse = response as? HTTPURLResponse else {
                print("Could not create URLResponse from request: \(self.request): \(String(describing: error))")
                DispatchQueue.main.async(execute: {
                    if let error = error {
                        self.failedToComplete(error.localizedDescription)
                    }
                    else {
                        self.failedToComplete("An unknown error has occured.")
                    }
                })
                
                return
            }
            
            print("Received URL response of \(urlResponse.statusCode) for URL: \(self.request.url!.absoluteString)")
            
            DispatchQueue.main.async(execute: {
                self.onCompletion(data, response, error)
            })
        }
        
        DispatchQueue.global().async {
            session.dataTask(with: self.request, completionHandler: completionHandler).resume()
        }
    }
}

