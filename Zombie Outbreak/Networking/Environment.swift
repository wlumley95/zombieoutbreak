//
//  Environment.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation

enum Environment: String
{
    case prod = "http://dmmw-api.australiaeast.cloudapp.azure.com:8080/"
}
