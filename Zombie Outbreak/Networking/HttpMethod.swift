//
//  HttpMethod.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 15/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation

enum HttpMethod: String
{
    case get    = "GET"
    case post   = "POST"
}
