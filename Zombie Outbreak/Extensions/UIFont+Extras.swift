//
//  UIFont+Extras.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 15/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import UIKit

extension UIFont
{
    public static let standard = UIFont.systemFont(ofSize: 18)
    public static let subtitle = UIFont.systemFont(ofSize: 20)
    public static let title = UIFont.systemFont(ofSize: 25)
    
    public static func standardFont(size: CGFloat) -> UIFont
    {
        return UIFont.systemFont(ofSize: size)
    }

    public static func boldFont(size: CGFloat) -> UIFont
    {
        return UIFont.boldSystemFont(ofSize: size)
    }
}
