//
//  Dictionary+HTTP.swift
//  Zombie Outbreak
//
//  Created by William Lumley on 16/6/20.
//  Copyright © 2020 William Lumley. All rights reserved.
//

import Foundation
import SwiftyJSON

extension Dictionary where Key == String
{
    public var jsonData: Data
    {
        return try! JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
    }
    
    /**
     Creates a string, and using this dictionary's keys and values, formats the string with key's and values in the
     format of a HTTP GET request. So if our dictionary contains these values:
         {"key": "value", "foo": "bar"},
     then we can expect this string to be returned:
         "?key=value&foo=bar"
     
     - returns: A string with parameters encoded within itself, in the HTTP GET request format
    */
    public func httpGetString() -> String
    {
        let keyCount = self.keys.count
        var i = 0
        
        var httpString = ""
        for (key, value) in self {
            //Add our key value to the url string
            httpString += "\(key)=\(value)"
            i += 1
            
            //If this is NOT the last key value, append the ampersand
            let isLastIteration = i >= keyCount
            if !isLastIteration {
                httpString += "&"
            }
        }
        
        return httpString
    }
}

